package npc.domain

import scala.util.Random

final case class AttributeSelection(
    name: String = "",
    values: Seq[String] = Nil,
    selection: Option[String] = None,
    visible: Boolean = true
) {
  def selectRandom(r: Random): AttributeSelection =
    copy(selection = Some(values(r.nextInt(values.length))))

  def toggle: AttributeSelection = copy(visible = !visible)

}

object AttributeSelection {
  def from(strings: Seq[String]): Option[AttributeSelection] =
    strings.map(_.trim).filter(_.nonEmpty).distinct.toList match {
      case name :: tail => Some(AttributeSelection(name, tail))
      case _            => None
    }

  val lastNameFantasy: AttributeSelection = AttributeSelection(
    "Nom",
    Seq(
      "Noki",
      "Baheut",
      "Breman",
      "Finemasse",
      "Shan",
      "Koltar",
      "Lazslo",
      "Lutran",
      "Sirgastin",
      "Moz",
      "Altimadre",
      "Toufaut",
      "Biskor",
      "Zacchèse",
      "Rabeude",
      "Badak",
      "Drim",
      "Orblantz",
      "Askamar",
      "Wortex",
      "Mastafiore",
      "Baccara",
      "El Vébeh",
      "Oc",
      "Bourak"
    )
  )

  val firstNameFantasy: AttributeSelection = AttributeSelection(
    "Prénom",
    Seq(
      "Iruld (H)",
      "Aubénia  (F)",
      "Liraine (F)",
      "Aenys (F)",
      "Lorinel (F)",
      "Arkibold (H)",
      "Garmon (H)",
      "Gardénia (F)",
      "Zolette (F)",
      "Naïs (F)",
      "Méruil (H)",
      "Chrysobelle (F)",
      "Vivelune (F)",
      "Yourik (H)",
      "Zora (F)",
      "Bziani (H)",
      "Aristéliod (H)",
      "Mélinöe (F)",
      "Gantyr (F)",
      "Ermenon (H)",
      "Mécassine (F)",
      "Bolmar (H)",
      "Tshikita (F)",
      "Aldor (H)",
      "Emile (H)",
      "Elianor (F)",
      "Béons (H)",
      "Causette (F)",
      "Odice (F)",
      "Arkab (H)",
      "Hercilia (F)",
      "Marissa (F)",
      "Lune (H/F)",
      "Mérithan (H)",
      "Obadiah (F)",
      "Shalara (F)",
      "Gusnav (H)",
      "Belchior (H)"
    )
  )

  val behaviour: AttributeSelection = AttributeSelection(
    "Comportement",
    Seq(
      "Sage, mentor, professeur",
      "Serviable, généreux",
      "Artisan, créatif, aime le travail bien fait",
      "Curieux, explorateur,apprenti",
      "Sérieux, chef, responsable",
      "Farceur, taquin, aime s’amuser",
      "Bavard, ragots, indic",
      "Naïf, crédule, groupie, suiveur",
      "Radin, cupide, près de ses sous",
      "Petit chef insupportable, dominant",
      "Malfaisant, mauvais",
      "Xénophobe",
      "Cherche le pouvoir",
      "Attaché à sa famille",
      "Blessé, veut se venger",
      "Protège la nature, l'équilibre",
      "Sens du commerce",
      "Téméraire, cherche la gloire",
      "Influençable, manipulé",
      "Affecteux, possessif, abusif",
      "Arrogant, orgueilleux, mégalo",
      "Brutal, cruel, sadique",
      "Charmeur, débauché, pervers",
      "Conformiste, réac, puritain",
      "Cynique, désabusé, malfaisant",
      "Désinvolte, sauvage, bestial",
      "Discipliné, autoritaire, tyrannique",
      "Distant, insensible, mysanthrope",
      "Docile, soumis, servile",
      "Egocentrique, narcissique, individualiste",
      "Excentrique, illuminé, mystique",
      "Fragile, maladif, morbide",
      "Impulsif, téméraire, suicidaire",
      "Irritable, aigri, haineux",
      "Mélancolique, défaitiste, dépressif",
      "Méthodique, manique, obsessionnel",
      "Nerveux, agité, frénétique",
      "Paresseux, passif, masochiste",
      "Passionné, acharché, fanatique",
      "Prudent, méfiant, paranoïaque",
      "Rebelle, révolé, asocial",
      "Rusé, sournois, diabolique",
      "Timide, anxieux, angoissé"
    )
  )

  val jobFantasy: AttributeSelection = AttributeSelection(
    "Métier",
    Seq(
      "Alchimiste",
      "Aubergiste",
      "Aventurier",
      "Bailli",
      "Boucher",
      "Boulanger",
      "Brasseur",
      "Charpentier",
      "Charretier",
      "Chasseur",
      "Chasseur de rats",
      "Cordonnier",
      "Courtisane",
      "Cueilleur",
      "Cuisinier",
      "Drapier",
      "Ecuyer",
      "Explorateur",
      "Fabricant de chandelles",
      "Fabricant de tonneaux",
      "Fauconnier",
      "Forgeron",
      "Fossoyeur",
      "Fourreur",
      "Garde",
      "Joailler",
      "Jongleur",
      "Maçon",
      "Marchant",
      "Ménestrel",
      "Meunier",
      "Musicien",
      "Noble",
      "Orfèvre",
      "Page",
      "Palefrenier",
      "Parfumeur",
      "Paysan",
      "Pêcheur",
      "Peintre",
      "Servant(e)",
      "Soldat",
      "Tailleur",
      "Valet",
      "Religieux",
      "Ermite",
      "Fermier",
      "Berger",
      "Chef du village / Maire",
      "Officier"
    )
  )
}
