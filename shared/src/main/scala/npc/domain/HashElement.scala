package npc.domain

final case class HashElement(
    attributeName: String,
    selection: String,
    visible: Boolean=true
)
