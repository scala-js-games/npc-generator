package npc.domain

import npc.domain.AttributesCollection._

import scala.util.Random
import scala.util.chaining.scalaUtilChainingOps

final case class AttributesCollection(attributes: Seq[AttributeSelection]) {
  def select(previous: AttributesCollection): AttributesCollection = {
    val selected = previous.attributes.collect {
      case AttributeSelection(name, _, Some(selection), _) =>
        HashElement(name, selection)
    }
    val visible = previous.attributes.collect {
      case a if a.visible => a.name
    }.toSet
    AttributesCollection(attributes.map { attribute =>
      selected
        .find(_.attributeName == attribute.name)
        .fold(attribute) { h => attribute.copy(selection = Some(h.selection)) }
        .pipe(a => a.copy(visible = a.visible || visible(attribute.name)))
    })
  }

  def apply(name: String): AttributeSelection =
    attributes.find(_.name == name).getOrElse(AttributeSelection())

  def updateAt(
      name: String,
      newAttr: AttributeSelection
  ): AttributesCollection =
    attributes.indexWhere(_.name == name) match {
      case -1    => copy(attributes = attributes :+ newAttr)
      case index => copy(attributes = attributes.updated(index, newAttr))
    }

  def selectRandom(random: Random): AttributesCollection =
    AttributesCollection(attributes.map(_.selectRandom(random)))

  val separator = "@@"
  def toHash: String =
    attributes
      .map(a =>
        a.name + (if (a.visible) "+" else "-") + a.selection.getOrElse("")
      )
      .mkString(separator)

  def withHash(hash: String): AttributesCollection = {
    val elements = hash.split(separator).toSeq.flatMap(parseElement)
    AttributesCollection(
      for {
        attribute <- attributes
        element = elements
          .find(sanitize(attribute.name) == _.attributeName)
          .getOrElse(HashElement(attribute.name, ""))
      } yield attribute.copy(
        visible = element.visible,
        selection = attribute.values.find(sanitize(_) == element.selection)
      )
    )
  }
}

object AttributesCollection {
  private val allowedChars = """0-9A-Za-zÀ-ÿ\s"""
  private val wordGroup    = s"([$allowedChars]+)"
  private val visibleRe    = s"$wordGroup\\+(.*)".r
  private val hiddenRe     = s"$wordGroup-(.*)".r

  private def parseElement(element: String): Option[HashElement] =
    element.replaceAll(s"""[^$allowedChars\\+\\-]""", "") match {
      case visibleRe(name, s) => Some(HashElement(name, s, true))
      case hiddenRe(name, s)  => Some(HashElement(name, s, false))
      case _                  => None
    }

  def sanitize(s: String): String = s.replaceAll(s"[^$allowedChars]", "")

  def sample: AttributesCollection = AttributesCollection(
    Seq(
      AttributeSelection.firstNameFantasy,
      AttributeSelection.lastNameFantasy,
      AttributeSelection.jobFantasy,
      AttributeSelection.behaviour
    )
  )

  def from(strings: Seq[Seq[String]]): Option[AttributesCollection] =
    strings
      .flatMap(AttributeSelection.from)
      .pipe(selections =>
        if (selections.nonEmpty) Some(AttributesCollection(selections))
        else None
      )
}
