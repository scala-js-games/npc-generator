package d20

import scala.annotation.tailrec
import scala.util.Random

object D20Simul  {
  implicit private val random: Random = Random
  val c1 = Character(
    "1",
    initiative = 14,
    maxHp = 20,
    defense = 12,
    damage = Damage(3, 13),
    bonusToHit = 4
  )
  val c2 = Character(
    "2",
    initiative = 14,
    maxHp = 20,
    defense = 12,
    damage = Damage(2, 12),
    bonusToHit = 3
  )

  println(format(stats(c1, c2, 100000)))

  def format(stats: Map[String, Int]): String = {
    val total = stats.values.sum
    stats.toSeq.sortBy(-_._2).map { case (name, wins) =>
      val ratio = wins * 100f / total
      f"$name : $ratio%.1f"
    }
  }.mkString("\n")

  def stats(a: Character, b: Character, count: Int): Map[String, Int] = {
    val winners = Seq.fill(count)(fight(a, b).name)
    winners.groupBy(identity).view.mapValues(_.size).toMap
  }

  @tailrec
  def fight(a: Character, b: Character): Character =
    if (a.ko) b
    else if (b.ko) a
    else {
      val (c, d) = oneRound(a, b)
      fight(c, d)
    }

  def oneRound(a: Character, b: Character): (Character, Character) = {
    val Seq(second, first) = random.shuffle(Seq(a, b)).sortBy(_.initiative)
    val secondDamaged      = second.attackedOnce(first)
    if (secondDamaged.ko) (first, secondDamaged)
    else (first.attackedOnce(second), secondDamaged)
  }
}

final case class Character(
    name: String,
    initiative: Int = 13,
    maxHp: Int = 10,
    defense: Int = 12,
    damage: Damage = Damage(3, 8),
    bonusToHit: Int = 3,
    receivedHits: Int = 0
) {
  def ko: Boolean = receivedHits >= maxHp

  def attackedOnce(that: Character)(implicit random: Random): Character =
    receive(that.hitDamage(defense))

  def receive(damage: Int): Character =
    copy(receivedHits = receivedHits + damage)

  def hitRoll(implicit random: Random): Int =
    random.nextInt(20) + 1 + bonusToHit

  def hitDamage(defense: Int)(implicit random: Random): Int =
    if (hitRoll >= defense)
      damage.damageRoll
    else 0

}

final case class Damage(min: Int, max: Int) {
  def damageRoll(implicit random: Random): Int =
    random.between(min, max + 1)
}
