package npc.domain

import npc.domain.ListOps._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ListSafeIndexTest extends AnyFlatSpec with Matchers {

  it should "return Some object when index in range" in {
    val actual = List(1, 2, 3).at(2)

    actual shouldBe Some(3)
  }

  it should "return None when index is too big" in {
    val actual = List(1, 2, 3).at(3)

    actual shouldBe None
  }

  it should "return None when index is < 0" in {
    val actual = List(1, 2, 3).at(-1)

    actual shouldBe None
  }

  it should "return None when empty" in {
    val actual = Nil.at(0)

    actual shouldBe None
  }
}
