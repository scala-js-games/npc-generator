package sheets

import com.raquo.laminar.api.L
import io.laminext.fetch.upickle._
import io.laminext.syntax.core._
import npc.domain.AttributesCollection
import org.scalajs.dom
import upickle.default._
import org.scalajs.macrotaskexecutor.MacrotaskExecutor.Implicits._

object Client {
  val parameters: String = Map(
    "majorDimension" -> "COLUMNS",
    "prettyPrint"    -> "false"
  ).map { case (k, v) =>
    s"$k=$v"
  }.mkString("&")

  def read(
      sheetId: String,
      range: String
  ): L.EventStream[AttributesCollection] =
    Fetch
      .get(
        url =
          s"https://sheets.googleapis.com/v4/spreadsheets/$sheetId/values/$range?$parameters",
        headers = Map("X-Goog-Api-Key" -> ApiKey.apiKey)
      )
      .decode[SheetsResponse]
      .map { r =>
        dom.window.localStorage.setItem(sheetId, write(r.data))
        AttributesCollection.from(r.data.values)
      }
      .collectDefined

}
