package sheets

import upickle.default._

object SheetsResponse {
  implicit val rw: ReadWriter[SheetsResponse] = macroRW[SheetsResponse]
}

case class SheetsResponse(range: String = "", values: Seq[Seq[String]] = Nil)
