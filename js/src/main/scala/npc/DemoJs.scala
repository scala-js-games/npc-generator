package npc

import com.raquo.laminar.api.L._
import npc.domain.AttributesCollection
import org.scalajs.dom
import org.scalajs.dom.document

import scala.scalajs.js.URIUtils
import scala.util.chaining.scalaUtilChainingOps

object DemoJs {

  def main(args: Array[String]): Unit = {
    document.addEventListener(
      "DOMContentLoaded",
      { (_: dom.Event) =>
        setupUI()
      }
    )
    dom.window.addEventListener(
      "hashchange",
      { (_: dom.Event) =>
        setupUI()
      }
    )
  }

  def setupUI(): Unit = {
    val body = dom.document.querySelector("body")
    body.innerHTML = ""

    val main = MainPage

    render(
      body,
      div(
        main.render,
        main.allSignal --> (s => HashUrl.writeHashToUrl(AttributesCollection(s).toHash))
      )
    )
  }


}

object HashUrl {
  def writeHashToUrl(hash: String): Unit =
    if (readHashFromUrl.isEmpty || readHashFromUrl.forall(hash.!=))
      dom.window.history.pushState("", "", "#" + hash)

  def readHashFromUrl: Option[String] = {
    val hash = dom.window.location.hash.drop(1).pipe(URIUtils.decodeURI).trim

    if (hash.isEmpty) None else Some(hash)
  }
}
