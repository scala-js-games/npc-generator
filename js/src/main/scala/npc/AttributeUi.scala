package npc

import com.raquo.laminar.api.L._
import npc.domain.AttributeSelection
import org.scalajs.dom.MouseEvent

import scala.util.Random

final case class AttributeUi(
    $selection: Var[AttributeSelection]
) {

  val nameSignal: Signal[String]     = $selection.signal.map(_.name)
  val visibleSignal: Signal[Boolean] = $selection.signal.map(_.visible)
  val display: Signal[String] =
    $selection.signal.map(a => if (a.visible) "" else "display:none")

  val render: HtmlElement =
    div(
      label(
        child <-- $selection.signal.map(_.name),
        forId <-- nameSignal,
        onClick --> (_ => $selection.update(_.toggle))
      ),
      button(
        "🎲",
        onClick --> Observer[MouseEvent](onNext = _ => selectRandom()),
        styleAttr <-- display
      ),
      select(
        idAttr <-- $selection.signal.map(_.name),
        option(value := "", ""),
        children <-- $selection.signal
          .map(_.values.sorted)
          .split(identity)((k, _, _) =>
            option(
              value := k,
              k,
              selected <-- $selection.signal
                .map(_.selection.contains(k))
                .setDisplayName(s"$k-selected")
            )
          ),
        onChange.mapToValue --> (s =>
          $selection.update(a => a.copy(selection = Some(s)))
        ),
        styleAttr <-- display
      ),
      input(
        `type` := "checkbox",
        onClick --> (_ => $selection.update(_.toggle)),
        checked <-- $selection.signal.map(_.visible)
      )
    )

  def selectRandom(): Unit =
    $selection.update(_.selectRandom(Random))

}
