package npc

import com.raquo.laminar.api.L._
import io.laminext.syntax.core.thisEvents
import npc.domain.AttributesCollection
import org.scalajs.dom
import sheets.{Client, SheetsResponse}
import upickle.default._

import scala.util.Try

final case class SheetUi($attributesCollection: Var[AttributesCollection]) {

  val defaultSheetId        = "1tH4UZQ73b-yIol-KmjYoD2GJ6yuh4TjcfyTZ-sNqw00"
  val $sheetId: Var[String] = Var(defaultSheetId)
  val $range: Var[String]   = Var("A:Z")

  val initialAttributes: EventStream[AttributesCollection] =
    EventStream
      .fromValue(defaultSheetId)
      .map { id =>
        val json = dom.window.localStorage.getItem(id)
        val resp = Try(read[SheetsResponse](json)).getOrElse(SheetsResponse())
        val fromSheet=AttributesCollection.from(resp.values)
        HashUrl.readHashFromUrl.fold(fromSheet)(h => fromSheet.map(_.withHash(h)))
      }
      .collect { case Some(ac) => ac }

  def render: HtmlElement = div(
    initialAttributes --> $attributesCollection,
    button(
      "refresh",
      thisEvents(onClick).andThen(_ => {
        val previous = $attributesCollection.now()
        Client.read($sheetId.now(), $range.now()).map(_.select(previous))
      }) --> $attributesCollection
    ),
    div(
      label(forId := "sheetId", "id Google Sheet"),
      input(
        `type` := "text",
        idAttr := "sheetId",
        value <-- $sheetId,
        onChange.mapToValue --> $sheetId
      )
    ),
    div(
      label(forId := "range", "range"),
      input(
        `type` := "text",
        idAttr := "range",
        value <-- $range,
        onChange.mapToValue --> $range
      )
    )
  )
}
