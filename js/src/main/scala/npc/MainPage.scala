package npc

import com.raquo.laminar.api.L._
import npc.domain.{AttributeSelection, AttributesCollection}

import scala.util.Random

object MainPage {

  val $attributesCollection: Var[AttributesCollection] = Var(
    AttributesCollection.sample
  )

  val allSignal: Signal[Seq[AttributeSelection]] =
    $attributesCollection.signal.map(_.attributes)

  def zoomForName(
      key: String
  )(implicit context: MountContext[?]): Var[AttributeSelection] =
    $attributesCollection.zoom(_.apply(key))((parent: AttributesCollection, derived: AttributeSelection) =>
      parent.updateAt(key, derived)
    )(context.owner)

  val render: HtmlElement = div(
    button(
      "🎲🎲",
      onClick --> (_ => $attributesCollection.update(_.selectRandom(Random)))
    ),
    div(onMountInsert { implicit ctx =>
      children <-- $attributesCollection.signal
        .map(_.attributes)
        .split(_.name)((key, _, _) => AttributeUi(zoomForName(key)).render)
    }),
    SheetUi($attributesCollection).render
  )

}
