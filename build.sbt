lazy val npcGenerator = project
  .in(file("."))
  .aggregate(npc.js,npc.jvm)
  .settings(
    publish      := {},
    publishLocal := {}
  )

lazy val npc = crossProject(JSPlatform, JVMPlatform)
  .in(file("."))
  .settings(
    name                                    := "npc",
    scalaVersion                            := "3.4.2",
    libraryDependencies += "org.scalatest" %%% "scalatest" % "3.2.18" % "test",
  )
  .jsSettings(
    scalaJSUseMainModuleInitializer := true,
    libraryDependencies ++= Seq(
      "com.raquo"    %%% "laminar"       % "17.0.0",
      "io.laminext"  %%% "fetch-upickle" % "0.17.0",
      "org.scala-js" %%% "scala-js-macrotask-executor" % "1.1.1"
    ),

  )


Global / onChangedBuildSource := ReloadOnSourceChanges
